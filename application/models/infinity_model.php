<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Infinity_model extends CI_Model{

	private $tweets_table = "tweets";
	private $cities_table = "cities";
	private $city_has_tweets_table = "city_has_tweets";
	private $last_tweet_table = "last_tweet_id";
	private $has = "#p3ngenpunya to";
    private $text = array(
        "grehreh #P3ngenPunya to jakarta",
        "#P3ngenPunya to bekasi",
        "#P3ngenPunya to surabaya",
        "grehreh grehreh #P3ngenPunya to jakarta dsgahae",
        "bandung #P3ngenPunya to", 
        "#P3ngenPunya to jakarta",
        "#P3ngenPunya to semarang reyhesrh rj",
        "#P3ngenPunya to bekasifb",
        "#P3ngenPunyato manado",
        "#P3ngenPunya surabaya",
        "#P3ngenPunya to jakarta",
    );
		
	function __construct(){
		parent::__construct();
	}

	function call_openGraph($graph_url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $graph_url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $output = curl_exec($ch);
        $output = json_decode($output);
        curl_close($ch);
        return $output;
    }

    function get_tweets(){
    	$last_tweet_id = $this->get_last_tweet_id();
    	$graph_url	= "http://magnum.tgrid.in/api/".$last_tweet_id;
		$result		= $this -> call_openGraph ($graph_url);

		if ($result->success == 1){
			return $result->data;
		}
		return $false;
    }

    function get_last_tweet_id(){
    	$sql = "SELECT `last_tweet_id` FROM ".$this->last_tweet_table." ORDER BY `last_tweet_id` DESC LIMIT 0,1";
    	$data = $this->db->query($sql);
        if ($data->num_rows == 0) {
            return 0;
        } else {
        	$result = $data->row_array();
        	return $result['last_tweet_id'];
        }
    }

    function parsing_data(){
    	$data_tweets = $this->get_tweets();
        if ($data_tweets){
            $success = 0;
            $failed = 0;
            foreach ($data_tweets as $key => $tweet) {
                $tweet_exits = $this->check_tweet_exists($tweet->tweet_id);
                if (!$tweet_exits){
                    $datatweets = array (
                        "tweet_id" => $tweet->tweet_id,
                        "screen_name" => $tweet->from_name,
                        "user_id" => $tweet->from_user_id,
                        "tweet_text" => $tweet->text,
                        "tweet_created_at" => $tweet->created_time,
                        "tweet_status" => "valid",
                        "created_at" => date("Y-m-d H:i:s")
                    );
                    
                    $oldtweettext = $this->text[$key]; //karena format tweetnya beda jadi pake fake tweet dulu
                    //$tweettext = $tweet->text; //kalo format tweetnya udah bener baru pake yang begini
                    
                    $tweettext = strtolower($oldtweettext);
                    $countpost = strpos($tweettext, $this->has);
                    $counthas = strlen($this->has); //menghitung jumlah karakter hastag sampai dengan to
                    $charmustberemove = $countpost + $counthas; //menghitung jumlah karakter yang akan di hapus

                    $newtweettext = substr($tweettext, 0, $charmustberemove);
                    $newtweettext = str_replace($newtweettext, "", $tweettext);
                    $artweet = $this->strmanipulate($newtweettext);

                    if ($this->db->insert($this->tweets_table, $datatweets)){
                        //ngecek apakah format hastagnya benar atau tidak
                        if ($countpost !== false){
                            //ngecek apakah posisi kotanya ada di belakang hastag atau tidak
                            if (!empty($artweet[0])){
                                $tweetstatus = "valid";
                                if ($this->update_tweet(array("tweet_status" => $tweetstatus), $tweet->tweet_id)){
                                    //ngecek apakah citynya sudah ada atau belum
                                    $city_exists = $this->check_city_exists($artweet[0]);
                                    if (!$city_exists){
                                        $datacity = array("city_name" => $artweet[0], "city_original_count" => 1, "created_at" => date("Y-m-d H:i:s"));
                                        if ($this->db->insert($this->cities_table, $datacity)) {
                                            $city_id = $this->db->insert_id();
                                            $datahastweet = array("tweet_id" => $tweet->tweet_id, "city_id" => $city_id);
                                            $this->db->insert($this->city_has_tweets_table, $datahastweet);
                                            $success = $success + 1;
                                        }else{
                                            return false;
                                        }
                                    }else{
                                        $datacity = array("city_name" => $artweet[0], "city_original_count" => $city_exists['city_original_count'] + 1, "modified_at" => date("Y-m-d H:i:s"));
                                        if ($this->update_city($datacity, $artweet[0])){
                                            $city_id = $city_exists['id'];
                                            $datahastweet = array("tweet_id" => $tweet->tweet_id, "city_id" => $city_id);
                                            $this->db->insert($this->city_has_tweets_table, $datahastweet);
                                            $success = $success + 1;
                                        }else{
                                            return false;
                                        } 
                                    }
                                }
                            }else{
                                $tweetstatus = "not-valid";
                                $this->update_tweet(array("tweet_status" => $tweetstatus), $tweet->tweet_id);
                                $failed = $failed + 1;
                            }         
                        }else{
                            $tweetstatus = "not-valid";
                            $this->update_tweet(array("tweet_status" => $tweetstatus), $tweet->tweet_id);
                            $failed = $failed + 1;
                        }

                        echo $oldtweettext." => ".$tweetstatus."<br/>";
                        
                    }else{
                        echo "failed";
                    }       
                    
                    $number = $key + 1;
                    if (count($data_tweets) == $number){
                        $datalasttweet = array("last_tweet_id" => $tweet->tweet_id, "date_insert" => date('Y-m-d H:i:s'));
                        $this->db->insert($this->last_tweet_table, $datalasttweet);
                        echo "Total tweet : ".count($data_tweets)." data valid : ".$success." data not-valid : ".$failed."<br/><br/>";
                    }
                }else{
                    return false;
                }
            }       
        }else{
            echo "tidak ada tweets";
        }
    	
    }

    function strmanipulate($s){
        $this->load->helper('url');
        $s = url_title($s);
        $r = explode("-", $s);
        return $r;
    }

    function check_tweet_exists($tweet_id){
    	$sql = "SELECT `tweet_id` FROM ".$this->tweets_table." WHERE `tweet_id` = ? ";
    	$data   = $this->db->query($sql, array($tweet_id));
    	if ($data->num_rows == 0){
    		return false;
    	}else{
    		return true;
    	}
    }

    function check_city_exists($city){
        $sql = "SELECT * FROM ".$this->cities_table." WHERE `city_name` = ? ";
        $data   = $this->db->query($sql, array($city));
        if ($data->num_rows == 0){
            return false;
        }else{
            $result = $data->row_array();
            return $result;
        }   
    }

    function update_city($datacity, $city_name){
        $this->db->where('city_name', $city_name);
        if ($this->db->update($this->cities_table, $datacity)){
            return true;
        }else{
            return false;
        }
    }

    function update_tweet($datatweet, $id){
        $this->db->where('tweet_id', $id);
        if ($this->db->update($this->tweets_table, $datatweet)){
            return true;
        }else{
            return false;
        }
    }
    
}