<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
    	parent::__construct();
        $this->load->model("infinity_model");
    }

    function index(){
    	$data = $this->infinity_model->parsing_data();
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */