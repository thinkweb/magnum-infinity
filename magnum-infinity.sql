/*
SQLyog Ultimate v9.50 
MySQL - 5.5.27 : Database - magnum_infinity
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`magnum_infinity` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `magnum_infinity`;

/*Table structure for table `cities` */

DROP TABLE IF EXISTS `cities`;

CREATE TABLE `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(45) DEFAULT NULL,
  `city_original_count` int(11) DEFAULT NULL,
  `city_manipulate_count` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Table structure for table `city_has_tweets` */

DROP TABLE IF EXISTS `city_has_tweets`;

CREATE TABLE `city_has_tweets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tweet_id` bigint(20) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

/*Table structure for table `last_tweet_id` */

DROP TABLE IF EXISTS `last_tweet_id`;

CREATE TABLE `last_tweet_id` (
  `last_tweet_id` bigint(20) NOT NULL,
  `date_insert` datetime DEFAULT NULL,
  PRIMARY KEY (`last_tweet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tweets` */

DROP TABLE IF EXISTS `tweets`;

CREATE TABLE `tweets` (
  `tweet_id` bigint(20) NOT NULL,
  `screen_name` varchar(45) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `tweet_text` varchar(200) DEFAULT NULL,
  `tweet_created_at` datetime DEFAULT NULL,
  `user_picture_url` varchar(200) DEFAULT NULL,
  `tweet_status` enum('not-set','valid','not-valid') DEFAULT 'not-set',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modifiet_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`tweet_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
